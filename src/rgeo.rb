#!/usr/bin/env ruby
require 'geocoder'

abort 'At least one argument must be supplied:  try a location ("Stuttgart") or an IP address ("139.162.203.138")' unless ARGV.count > 0
result = Geocoder.search( ARGV[0] ).first
puts "  #{ARGV[0]}: #{result.city}, #{result.state}. Lat/Long: #{result.coordinates}"
